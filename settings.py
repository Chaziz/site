DB = 'sqlite:///../server/escargot.sqlite'
STATS_DB = 'sqlite:///../server/stats.sqlite'
DEBUG = False
PORT = 8080

SITE_LINK_PASSWORD = 'password'

DP_HOST = 'm1.escargot.log1p.xyz'


# Set this in settings_local to use reCAPTCHA
RECAPTCHA = {
	'api_key': None,
	'secret_key': None,
}

# Set this in `settings_local` to use SendGrid
SENDGRID_API_KEY = None

try:
	from settings_local import *
except ImportError:
	raise Exception("Please create settings_local.py")
